<?php

namespace Drupal\commerce_cart_flyout\Normalizer;

use Drupal\commerce_product\Entity\ProductAttributeValueInterface;
use Drupal\commerce_product\PreparedAttribute;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\serialization\Normalizer\NormalizerBase;

/**
 * Provides a normalizer for prepared attributes.
 */
class PreparedAttributeNormalizer extends NormalizerBase {

  /**
   * The interface or class that this Normalizer supports.
   *
   * @var string
   */
  protected $supportedInterfaceOrClass = 'Drupal\commerce_product\PreparedAttribute';

  /**
   * Constructs a new PreparedAttributeNormalizer object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected RendererInterface $renderer,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []): float|int|bool|\ArrayObject|array|string|null {
    if (!$object instanceof PreparedAttribute) {
      return '';
    }

    $data = $object->toArray();
    $attribute_ids = array_keys($data['values']);
    /** @var \Drupal\commerce_product\ProductAttributeValueStorageInterface $attribute_value_storage */
    $attribute_value_storage = $this->entityTypeManager->getStorage('commerce_product_attribute_value');
    /** @var \Drupal\commerce_product\Entity\ProductAttributeValueInterface[] $attributes */
    // @todo make sure loaded in same langcode.
    $attributes = array_reduce($attribute_value_storage->loadMultiple($attribute_ids), function ($carry, ProductAttributeValueInterface $value) {
      $carry[] = $value;
      return $carry;
    }, []);

    if ($object->getElementType() == 'commerce_product_rendered_attribute') {
      $view_builder = $this->entityTypeManager->getViewBuilder('commerce_product_attribute_value');
      foreach ($attributes as $key => $value) {
        $rendered_attribute = $view_builder->view($value, 'add_to_cart');
        $attributes[$key]->rendered = $this->renderer->render($rendered_attribute);
      }
    }

    $data['values'] = $this->serializer->normalize($attributes);

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function hasCacheableSupportsMethod(): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedTypes(?string $format): array {
    return [
      PreparedAttribute::class => TRUE,
    ];
  }

}
