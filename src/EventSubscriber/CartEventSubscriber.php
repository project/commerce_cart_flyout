<?php

namespace Drupal\commerce_cart_flyout\EventSubscriber;

use Drupal\commerce_cart\Event\CartEntityAddEvent;
use Drupal\commerce_cart\EventSubscriber\CartEventSubscriber as CommerceCartEventSubscriber;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Replaces the original CartEventSubscriber from commerce_cart module.
 *
 * This prevents `_cart_api` routes from triggering messages.
 */
class CartEventSubscriber extends CommerceCartEventSubscriber {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public function displayAddToCartMessage(CartEntityAddEvent $event) {
    if (!$this->routeMatch->getRouteObject()->hasRequirement('_cart_api')) {
      parent::displayAddToCartMessage($event);
    }
  }

  /**
   * Sets the current route match service.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route_match
   *   The new current route match service.
   *
   * @return $this
   */
  public function setCurrentRouteMatch(RouteMatchInterface $current_route_match) {
    $this->routeMatch = $current_route_match;
    return $this;
  }

}
